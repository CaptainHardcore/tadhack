package com.itkindaworks.tadhack;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by remember me on 23.09.2017.
 */

public class ChatAdapter  extends RecyclerView.Adapter<ChatAdapter.VievHolder>{

    public ChatAdapter(ArrayList<String> messages) {
        this.messages = messages;
    }

    ArrayList<String> messages;
    @Override
    public ChatAdapter.VievHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
        return new VievHolder(v);
    }

    @Override
    public void onBindViewHolder(ChatAdapter.VievHolder holder, int position) {
holder.text.setText(messages.get(position));
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

     class VievHolder extends RecyclerView.ViewHolder{
         TextView text;
         public VievHolder(View itemView) {
             super(itemView);
             text = itemView.findViewById(R.id.text_message);
         }
     }
}
