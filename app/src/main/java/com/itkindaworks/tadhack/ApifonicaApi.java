package com.itkindaworks.tadhack;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by remember me on 23.09.2017.
 */

public interface ApifonicaApi {
    @FormUrlEncoded
    @POST("/v2/accounts/acc04918e83-deda-3a44-a41f-c7083a1de803/messages")
    Call<SMSResponce> sendSMS(
                              @Field("from")String from,
                              @Field("to")String to,
                              @Field("text")String text
                              );
}
