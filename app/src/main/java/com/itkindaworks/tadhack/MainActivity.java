package com.itkindaworks.tadhack;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

import de.adorsys.android.smsparser.SmsConfig;
import de.adorsys.android.smsparser.SmsReceiver;
import de.adorsys.android.smsparser.SmsTool;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private Button mButton;
    private Button mParse;
    private EditText number;
    private EditText text;
    private TextView smsSenderTextView;
    private TextView smsMessageTextView;
    LocalBroadcastManager localBroadcastManager;
    BroadcastReceiver broadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        number = (EditText)findViewById(R.id.number_input);
        smsMessageTextView = (TextView)findViewById(R.id.sended);
        smsSenderTextView = (TextView)findViewById(R.id.received);
        text  = (EditText)findViewById(R.id.text_input);
        mButton = (Button) findViewById(R.id.send);
        mParse = (Button)findViewById(R.id.parse);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            SmsTool.requestSMSPermission(MainActivity.this);
        }
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        OkHttpClient client = new OkHttpClient();

                        MediaType mediaType = MediaType.parse("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
                        RequestBody body = RequestBody.create(mediaType, "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"from\"\r\n\r\n3584573975884\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"to\"\r\n\r\n"+number.getText().toString()+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"text\"\r\n\r\n"+text.getText().toString()+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--");
                        Request request = new Request.Builder()
                                .url("https://api.apifonica.com/v2/accounts/acc04918e83-deda-3a44-a41f-c7083a1de803/messages")
                                .post(body)
                                .addHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
                                .addHeader("authorization", "Basic YWNjMDQ5MThlODMtZGVkYS0zYTQ0LWE0MWYtYzcwODNhMWRlODAzOmF1dGJkN2ZjYzYzLWYxYTAtMzlkYy1hOWJiLTUwYmQ2MjkwZjllOQ==")
                                .addHeader("cache-control", "no-cache")
                                .build();

                        try {
                            Response response = client.newCall(request).execute();
                            response.body();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                t.start();


            }
        });
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(SmsReceiver.INTENT_ACTION_SMS)) {
                    String receivedSender = intent.getStringExtra(SmsReceiver.KEY_SMS_SENDER);
                    String receivedMessage = intent.getStringExtra(SmsReceiver.KEY_SMS_MESSAGE);
                    smsSenderTextView.setText(
                            receivedSender != null ? receivedSender : "NO NUMBER");
                    smsMessageTextView.setText(
                            receivedMessage != null ? receivedMessage : "NO MESSAGE");
                }
            }
        };


        SmsConfig.INSTANCE.initializeSmsConfig(
                "A",
                "B",
                "Outbox", "3584573975884", "+3584573975884");
        mParse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ChatActivity.class);
                intent.putExtra("number", number.getText().toString());
                startActivity(intent);

            }
        });





    }

    @Override
    protected void onResume() {
        registerReceiver();
        super.onResume();
    }

    @Override
    protected void onPause() {
        unRegisterReceiver();
        super.onPause();
    }

    private void registerReceiver() {
        localBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsReceiver.INTENT_ACTION_SMS);
        localBroadcastManager.registerReceiver(broadcastReceiver, intentFilter);
    }

    private void unRegisterReceiver() {
        localBroadcastManager.unregisterReceiver(broadcastReceiver);
    }


}
