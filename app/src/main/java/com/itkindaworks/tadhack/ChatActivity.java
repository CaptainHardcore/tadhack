package com.itkindaworks.tadhack;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import java.io.IOException;
import java.util.ArrayList;

import de.adorsys.android.smsparser.SmsConfig;
import de.adorsys.android.smsparser.SmsReceiver;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by remember me on 23.09.2017.
 */

public class ChatActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private ImageButton mImageButton;
    private EditText mEditText;
    ArrayList<String> mStrings;
    String recepient;
    LocalBroadcastManager localBroadcastManager;
    BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        //region V setup
        mRecyclerView = (RecyclerView)findViewById(R.id.recycler);
        mEditText = (EditText)findViewById(R.id.input_message);
        mImageButton = (ImageButton)findViewById(R.id.send_btn);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setStackFromEnd(true);
        mStrings = new ArrayList<>();
        final ChatAdapter adapter = new ChatAdapter(mStrings);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setAdapter(adapter);
        Intent intent = getIntent();
        recepient = intent.getStringExtra("number");
        //endregion
        mImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditText.setText("");
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        OkHttpClient client = new OkHttpClient();

                        MediaType mediaType = MediaType.parse("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
                        RequestBody body = RequestBody.create(mediaType, "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"from\"\r\n\r\n3584573975884\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"to\"\r\n\r\n"+recepient+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"text\"\r\n\r\n"+"A\n"+mEditText.getText().toString()+"\n"+"B\n"+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--");
                        Request request = new Request.Builder()
                                .url("https://api.apifonica.com/v2/accounts/acc04918e83-deda-3a44-a41f-c7083a1de803/messages")
                                .post(body)
                                .addHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
                                .addHeader("authorization", "Basic YWNjMDQ5MThlODMtZGVkYS0zYTQ0LWE0MWYtYzcwODNhMWRlODAzOmF1dGJkN2ZjYzYzLWYxYTAtMzlkYy1hOWJiLTUwYmQ2MjkwZjllOQ==")
                                .addHeader("cache-control", "no-cache")
                                .build();

                        try {
                            Response response = client.newCall(request).execute();
                            response.body();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                t.start();
            }
        });

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(SmsReceiver.INTENT_ACTION_SMS)) {

                    String receivedSender = intent.getStringExtra(SmsReceiver.KEY_SMS_SENDER);
                    String receivedMessage = intent.getStringExtra(SmsReceiver.KEY_SMS_MESSAGE);

                    mStrings.add(
                            receivedMessage != null ? receivedMessage : "NO MESSAGE");
                    adapter.notifyDataSetChanged();
                    mRecyclerView.setAdapter(adapter);

                }
            }
        };


        SmsConfig.INSTANCE.initializeSmsConfig(
                "A",
                "B",
                "Outbox", "3584573975884", "+3584573975884");


    }

    @Override
    protected void onResume() {
        registerReceiver();
        super.onResume();
    }

    @Override
    protected void onPause() {
        unRegisterReceiver();
        super.onPause();
    }

    private void registerReceiver() {
        localBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsReceiver.INTENT_ACTION_SMS);
        localBroadcastManager.registerReceiver(broadcastReceiver, intentFilter);
    }

    private void unRegisterReceiver() {
        localBroadcastManager.unregisterReceiver(broadcastReceiver);
    }

}
